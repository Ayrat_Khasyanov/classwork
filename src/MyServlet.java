import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;


@WebServlet("/test")
public class MyServlet extends HttpServlet {
    private Configuration cfg;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        doGet(request,response);
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html; charset=utf8");

        String firstName = request.getParameter("firstName");
        String secondName = request.getParameter("secondName");

        try {
            PrintWriter out = response.getWriter();

            cfg.setServletContextForTemplateLoading(request.getServletContext(), "/ftl");

            Map<String, Object>  helloMap = new HashMap<String, Object>();
            helloMap.put("name", firstName+" "+secondName);

            Template temp = cfg.getTemplate("hello.ftl");
            temp.process(helloMap, out);

            out.close();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void init(){
        cfg = new Configuration(Configuration.VERSION_2_3_26);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        cfg.setLogTemplateExceptions(false);
    }

}
